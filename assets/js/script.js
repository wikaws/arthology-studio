var modelViewer = document.querySelector('model-viewer');
var descElement = document.querySelector(".c-wrapper").offsetHeight;
var btnScanElement = document.querySelector(".c-button-wrapper").offsetHeight;
var arButton = document.querySelector('#ar-button');
var scaleButtonWrapper = document.querySelector('.c-button-scale');
var scaleButton = document.querySelector('#btnScale');
var scaleResetButton = document.querySelector('#btnResetScale');

// Handles loading the events for <model-viewer>'s slotted progress bar
const onProgress = (event) => {
  const progressBar = event.target.querySelector('.progress-bar');
  const updatingBar = event.target.querySelector('.update-bar');
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};

const handleARButtonClick = () => {
  if (modelViewer.canActivateAR) {
    modelViewer.activateAR();
  } else {
    console.error('AR mode is not supported.');
    alert('AR mode is not supported on your device');
  }
};

const updateScale = () => {
  modelViewer.scale = `${1} ${1} ${1}`;
};

const resetScale = () => {
  modelViewer.scale = `${0.1} ${0.1} ${0.1}`;
};

document.querySelector('model-viewer').addEventListener('progress', onProgress);
document.getElementById("ar-button").style.bottom = descElement - (btnScanElement - 18) + "px";

document.querySelector("details").addEventListener("toggle", function() {
 document.getElementById("ar-button").style.bottom = document.querySelector(".c-wrapper").offsetHeight - (document.querySelector(".c-button-wrapper").offsetHeight - 18) + "px";
})

arButton.addEventListener('click', handleARButtonClick)
scaleButton.addEventListener('click', updateScale)
scaleResetButton.addEventListener('click', resetScale)

document.querySelector("#exitAR").addEventListener('click', async () => {
  await resetScale()
});

// modelViewer.addEventListener('load', () => {
//   if(object3D != null){
//     if (modelViewer.canActivateAR) {
//       modelViewer.activateAR();
//     } else {
//       console.error('AR mode is not supported.');
//     }
//   }
// });

